// program of printing triangle structure using recursion



package Assignments;

import java.util.Scanner;


public class PrintTringle_Recursion {
	

static void printn(int i)
{
	if(i==0)
		return;
	System.out.print("* ");
	
	printn(i-1);
}
	

static void pattern(int n, int i)
{
	if(n==0)
		return;
	printn(i);
	System.out.println("");
	pattern(n-1,i+1);
}
	
	public static void main(String arg[])
	{
		System.out.println("Enter how many rows you want: ");
		
		
    	Scanner sc=new Scanner(System.in);
    	int n=sc.nextInt();
    	pattern(n,1);
	}
}
