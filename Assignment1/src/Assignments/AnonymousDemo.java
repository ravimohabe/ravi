package Assignments;

public class AnonymousDemo {

  public static void main(String arg[]) {

    Thread t = new Thread() {
      public void run() {
        System.out.println("Thread is running   ");
      }
    };
    t.start();
    System.out.println("You are in main thread");
  }

}
