package Assignments;

@Course(cid = "B1", cname = "java", ccost = 10000)
public class Student {
  String sid;
  String sname;
  String saddr;

  public Student(String sid, String sname, String saddr) {
    super();
    this.sid = sid;
    this.sname = sname;
    this.saddr = saddr;
  }

  public void getStudentDetails() {
    System.out.println("Student details:" + "\nStudent Id:" + sid + "\nStudent name:" + sname
        + "\nStudent address:" + saddr);
  }
}
