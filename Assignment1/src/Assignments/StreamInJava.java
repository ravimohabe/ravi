package Assignments;

import java.util.Arrays;
import java.util.stream.IntStream;

public class StreamInJava {
  public static void main(String str[]) {
    // Printing integer from 1 to 9
    IntStream.range(1, 10).forEach(System.out::print);
    System.out.println();

    // print element in given range by skipping first four element
    IntStream.range(1, 10).skip(4).forEach(System.out::print);

    // From given list print all name by starting with "A"
    String[] names = {"Aditya", "Akshay", "Ravi", "Prashant", "Kaivalya", "Pradamesh", "Mohit"};
    Arrays.stream(names).filter(x -> x.startsWith("A")).sorted().forEach(System.out::print);



  }
}
