package Assignments;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class AceptDateAndTimeSep {

  public static void main(String arg[]) {
    System.out.println("Enter your Date(yyyy-mm-ss): ");

    Scanner sc = new Scanner(System.in);
    String dstr = sc.nextLine();

    System.out.println("Enter your Time(HH:MM:SS): ");
    String tstr = sc.nextLine();

    Date d = null;
    Date t = null;
    Date dt = null;

    SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

    try {
      d = format.parse(dstr);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println("Your date: " + d);

    format = new SimpleDateFormat("HH:mm:ss");

    try {
      t = format.parse(tstr);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println("Your Time: " + t);

    String dtstr = dstr + " " + tstr;

    format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    try {
      dt = format.parse(dtstr);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println("yyyy-MM-dd'T'HH:mm:ss" + dt);


  }


}
