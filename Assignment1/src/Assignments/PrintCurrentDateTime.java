package Assignments;

import java.util.Date;
import java.text.SimpleDateFormat;

public class PrintCurrentDateTime {
  public static void main(String arg[]) {
    Date date = new Date();
    System.out.println(date);
    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    String strdate = formatter.format(date);
    System.out.println("MM/dd/yyyy HH:mm:ss: " + strdate);


    formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    strdate = formatter.format(date);
    System.out.println("yyyy-MM-dd'T'HH:mm:ssZ: " + strdate);

    formatter = new SimpleDateFormat("hh:mm a");
    strdate = formatter.format(date);
    System.out.println("hh:mm a: " + strdate);

    formatter = new SimpleDateFormat("HH:mm:ss");
    strdate = formatter.format(date);
    System.out.println("HH:mm:ss: " + strdate);

    formatter = new SimpleDateFormat("MMM dd yyyy");
    strdate = formatter.format(date);
    System.out.println("MMM dd yyyy: " + strdate);
  }
}
