package Assignments;


public class Practice {
public static String createRegion(String str, int start, int end){
if(start < 0 || end < 0 || start > end || end > str.length()){
System.out.println("Index out of bounds");
return null;
}

String region = "";

for(int i = start; i < end; i++){
region += str.charAt(i);
}

return region;
}

public static boolean equalRegion(String region1, String region2){
if(region1.equals(region2)){
return true;
}else
return false;
}

public static void main(String[] args){
String string1 = "I love coding in Java";
String string2 = "I really love coding in Java";
String string1Region = createRegion(string1, 2, 6);
String string2Region = createRegion(string2, 2, 6);
String string1Region2 = createRegion(string1,7, string1.length());
String string2Region2 = createRegion(string2,14, string2.length());
System.out.println("String1 region == String2 region? " + equalRegion(string1Region, string2Region));
System.out.println("String1 region == String2 region? " + equalRegion(string1Region2, string2Region2));
}
}





