// program of printing triangle Structure using Single loop


package Assignments;

import java.util.Scanner;


public class Triangle_Single_Loop {

	public static void main(String str[])
	{
		int line_no=1, cur_star=0;
		
		
		System.out.println("Enter no of line you want");
		
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		
		
		while(line_no <= n) 
        { 
            
            if (cur_star < line_no) 
            { 
                System.out.print ( "* "); 
                cur_star++; 
                continue; 
            } 
      
       
            if (cur_star == line_no) 
            { 
                System.out.println (""); 
                line_no++; 
                cur_star = 0; 
            } 
        }
			
	}
	
}
