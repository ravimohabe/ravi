package Assignments;

import java.util.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;


public class CollectonSet {

  public static LinkedHashMap<Integer, String> sortMap(LinkedHashMap<Integer, String> map) {
    List<Map.Entry<Integer, String>> nameList = new LinkedList<>(map.entrySet());
    Collections.sort(nameList, (o1, o2) -> o1.getValue().compareTo(o2.getValue()));
    LinkedHashMap<Integer, String> result = new LinkedHashMap<>();
    for (Map.Entry<Integer, String> entry : nameList) {

      result.put(entry.getKey(), entry.getValue());
    }
    return result;
  }

  public static void DeleteElement(LinkedHashMap<Integer, String> hMap, String strN) {
    int i = 0;
    for (Map.Entry<Integer, String> entry : hMap.entrySet()) {
      if (strN.equals(entry.getValue())) {
        i = entry.getKey();
        break;
      }
    }
    hMap.remove(i);
  }


  public static void main(String[] Args) {
    Scanner reader = new Scanner(System.in);
    LinkedHashMap<Integer, String> hMap = new LinkedHashMap<Integer, String>();
    String sName;
    HashSet<String> names = null;
    int iLimit, iCnt = 0, i = 0;
    try {

      boolean loop = true;
      while (loop == true) {
        System.out.println(
            "1.Enter names\n2.List all Elemnets(keys+values)\n3.Sort by values\n4.Delete a value\n5.Avalibility of value\n6.Check for empty collection\n0.Exit\nEnter your choise : ");
        int iChoise = Integer.parseInt(reader.nextLine());
        switch (iChoise) {
          case 0:
            loop = false;
            break;
          case 1:
            System.out.println("How meny elements you want to insert ");
            iLimit = Integer.parseInt(reader.nextLine());
            names = new HashSet<String>();
            while (iCnt < iLimit) {
              System.out.println("Enter name " + (iCnt + 1));
              sName = reader.nextLine();
              if (names.add(sName)) {
                System.out.println("addition successful");
                iCnt++;
              } else {
                System.out.println("addition failed " + iCnt);
              }
            }
            iCnt = 0;
            System.out.println("\nConverting HashSets to HashMaps\n");
            hMap = new LinkedHashMap<Integer, String>();
            for (String str : names) {
              hMap.put(i, str);
              i++;
            }
            i = 0;
            break;
          case 2:
            System.out.println("Listing of key and value pair");
            if (hMap.isEmpty() == true) {
              System.out.println("hash table is Empty");
            } else {
              System.out.println(hMap);
            }
            break;
          case 3:
            System.out.println("\nmap after sorting by values: ");
            System.out.println(sortMap(hMap));
            break;
          case 4:
            System.out.println("\nEnetr the name of element to be deleted ");
            String strN = reader.nextLine();
            DeleteElement(hMap, strN);
            System.out.println("Elements after deleting " + hMap);
            break;
          case 5:
            System.out.println("\nEnetr the value to be checked ");
            strN = reader.nextLine();
            if (hMap.containsValue(strN) == true) {
              System.out.println("hash table do have " + strN + " as value");
            } else {
              System.out.println("hash table do not have " + strN + " as value");
            }
            break;
          case 6:
            if (hMap.isEmpty() == true) {
              System.out.println("hash table is Empty");
            } else {
              System.out.println("hash table is not Empty");
            }
            break;
          default:
            System.out.println("Wrong Choise");
        }
      }
    } catch (Exception e) {
      System.out.println(e);
    }
    reader.close();
  }
}
