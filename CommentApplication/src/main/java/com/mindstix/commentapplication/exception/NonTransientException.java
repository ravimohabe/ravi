package com.mindstix.commentapplication.exception;

import org.springframework.dao.NonTransientDataAccessException;

public class NonTransientException extends NonTransientDataAccessException {

  public NonTransientException(String exception) {
    super(exception);
  }
}
