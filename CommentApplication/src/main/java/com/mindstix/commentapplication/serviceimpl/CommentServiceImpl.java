package com.mindstix.commentapplication.serviceimpl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import com.mindstix.commentapplication.dao.CommentDao;
import com.mindstix.commentapplication.exception.DatabaseException;
import com.mindstix.commentapplication.exception.ResourseNotFoundException;
import com.mindstix.commentapplication.model.Comment;
import com.mindstix.commentapplication.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {

  final private static Logger LOGGER = LoggerFactory.getLogger(CommentServiceImpl.class);

  @Autowired
  CommentDao commentDao;

  /**
   * This function is used to get list of all comment
   * 
   * @return List of all comment
   */
  @Override
  public List<Comment> getComment() {
    LOGGER.info("Control came to getComment function");
    List<Comment> comment = commentDao.findAll();
    return comment;
  }

  /**
   * This function is used to comment
   * 
   * @param comment
   */
  @Override
  public void addComment(Comment comment) {
    LOGGER.info("Control came to addComment function");
    commentDao.save(comment);
  }

  /**
   * This function is to get all comment by user id
   * 
   * @param user id
   * @return List of comment
   */
  @Retryable(value = {DatabaseException.class, ResourseNotFoundException.class}, maxAttempts = 2,
      backoff = @Backoff(delay = 5000))
  @Override
  public List<Comment> getCommentByUserId(Integer id) {
    LOGGER.info("Control came to getCommentByUserId function with userid={}",id);
    List<Comment> comment = null;
    comment = commentDao.findAllCommentByUserid(id);
    if (comment.isEmpty()) {
      throw new ResourseNotFoundException("No Comment present with user id=" + id);
    } else {
      return comment;
    }
  }

  /**
   * This function is used to a delete comment by comment id
   * 
   * @param Comment id
   * @return return deleted comment
   */
  @Override
  public Comment deleteCommentByCommentId(Integer id) {
    LOGGER.info("Control came to deleteCommentByCommentId function");
    Comment newComment = null;
    try {
      List<Comment> comment = commentDao.findAll();
      newComment = comment.stream().filter(a -> a.getCommentid().equals(id)).findFirst().get();
      commentDao.deleteComment(newComment);
      LOGGER.info("Comment with comment id={} deleted succefully", id);
      return newComment;
    } catch (Exception e) {
      LOGGER.info("No Comment found with Comment id={}", id);
      throw new ResourseNotFoundException("No comment found with comment id=" + id);
    }
  }
}
