package com.mindstix.commentapplication.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mindstix.commentapplication.constant.CommentConstant;
import com.mindstix.commentapplication.model.Comment;
import com.mindstix.commentapplication.service.CommentService;


@RestController
public class CommentController {

  final private static Logger LOGGER = LoggerFactory.getLogger(CommentController.class);
  @Autowired
  private CommentService commentService;

  /**
   * This API is Used to get all comment
   * 
   * @return this API return list of comment
   */
  @RequestMapping(value = CommentConstant.COMMENT)
  List<Comment> getComment() {
    LOGGER.info("Request received to get all list of Comment ");
    List<Comment> comment = commentService.getComment();
    return comment;
  }

  /**
   * This API is used to add comment
   * 
   * @param comment
   */
  @RequestMapping(method = RequestMethod.POST, value = CommentConstant.COMMENT)
  void addComment(@RequestBody Comment comment) {
    LOGGER.info("Request received  add coomment ");
    commentService.addComment(comment);
  }

  /**
   * This API fetch List Of All Comment by given user id
   * 
   * @param id user id
   * @return list of comment
   */
  @RequestMapping(value = CommentConstant.COMMENTBYUSEID)
  List<Comment> getCommentByUserId(@PathVariable int id) {
    LOGGER.info("Request received to get all comment by given user id={}",id);
    List<Comment> commentByUser = commentService.getCommentByUserId(id);
    return commentByUser;
  }

  /**
   * This API is used to add comment
   * 
   * @param comment
   */
  @RequestMapping(value = CommentConstant.COMMENTBYUSE, method = RequestMethod.POST)
  void addCommentByUser(@RequestBody Comment comment) {
    LOGGER.info("Request received to add comment ");
    commentService.addComment(comment);
  }

  /**
   * This API is used to delete comment
   * 
   * @param id comment id
   * @return return deleted comment
   */
  @RequestMapping(value = CommentConstant.COMMENTBYCOMMENTID, method = RequestMethod.DELETE)
  Comment deleteCommentByCommentId(@PathVariable Integer id) {
    LOGGER.info("Request received delete comment with comment id={}", id);
    Comment deleteComment = commentService.deleteCommentByCommentId(id);
    return deleteComment;
  }
}
