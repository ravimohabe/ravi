package com.mindstix.userapplication.daoimpl;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import com.mindstix.userapplication.dao.UserDao;
import com.mindstix.userapplication.exception.DatabaseException;
import com.mindstix.userapplication.exception.NonTransientException;
import com.mindstix.userapplication.exception.ResourseNotFoundException;
import com.mindstix.userapplication.exception.TransientException;
import com.mindstix.userapplication.model.User;
import com.mindstix.userapplication.repository.UserRepository;
import com.mindstix.userapplication.service.UserService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Controller
public class UserDaoImpl implements UserDao {

  final private static Logger LOGGER = LoggerFactory.getLogger(UserService.class);
  @Autowired
  private UserRepository userRepository;

  /**
   * This function is to fetch all user from database
   * 
   * @return return list of users
   */
  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {TransientException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public List<User> findAll() {
    LOGGER.info("getting user from database");
    try {
      List<User> users = userRepository.findAll();
      return users;
    } catch (TransientException e) {
      throw new TransientException("Non able to find all error");
    } catch (NonTransientException e) {
      throw new NonTransientException("Non transient Exception occure");
    }
  }



  /**
   * This function is used to save user to database
   * 
   * @param user
   */
  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {TransientException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public void save(User user) throws SQLException {
    LOGGER.info("Saving user to database");
    try {
      userRepository.save(user);
    } catch (TransientException e) {
      throw new TransientException("transient Exception occure");
    } catch (NonTransientException e) {
      throw new NonTransientException("Non transient exception Occure ");
    }
  }

  /**
   * This function is used to find user by given id
   * 
   * @param User id
   * @return return user
   */

  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {TransientException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public User findById(int id) {
    LOGGER.info("Serching for user with user id={}", id);
    User user = null;
    try {
      user = userRepository.findByUserid(id);
    } catch (TransientException e) {
      throw new TransientException("Transient Exception Occure");
    } catch (NonTransientException e) {
      throw new NonTransientException("Non transient exception occure");
    }

    if (user == null) {
      LOGGER.info("NO user present with user  id={}", id);
      throw new ResourseNotFoundException("No user found with user id=" + id);
    } else
      return user;
  }

}
