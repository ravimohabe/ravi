package com.mindstix.userapplication.constant;

public class UserConstant {
  public final static String USERURL = "/user";
  public final static String USERID = "/{id}";
  public final static String COMMENTBYUSERID = "/{id}/comment";
  public final static String COMMENT = "/comment";
  public final static String COMMENTID = "/comment/{id}";
  public final static String COMMENTBYID = "/comment/user/{id}";
  public final static String USERCOMMENT = "/comment/user";
}