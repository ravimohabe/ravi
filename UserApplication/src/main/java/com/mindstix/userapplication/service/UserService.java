package com.mindstix.userapplication.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import com.mindstix.userapplication.model.Comment;
import com.mindstix.userapplication.model.User;

public interface UserService {

  /**
   * this function returns list of all users
   * 
   * @return List of users
   */
  public List<User> getUser();

  /**
   * this function used to add user
   * 
   * @param user
   * @throws SQLException
   */
  public void addUser(User user) throws SQLException;

  /**
   * this function is used to get all comment by user id
   * 
   * @param id user id
   * @return list of comment
   */
  public List<Comment> getCommentByUserId(int id);

  /**
   * this function is used to add comment
   * 
   * @param comment
   */
  public void addComment(Comment comment);

  /**
   * this function is used to get user by given id
   * 
   * @param id user id
   * @return user present with given id
   */
  public User getUserById(int id);

  /**
   * this function to used to delete comment by user
   * 
   * @param id
   */
  public void deleteComment(Integer id);

}
