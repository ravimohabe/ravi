package com.mindstix.userapplication.exception;

import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

  @ExceptionHandler(GlobalException.class)
  public ResponseEntity<Object> globalExeption(GlobalException ex) {
    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage());
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourseNotFoundException.class)
  public ResponseEntity<Object> resourseNotFoundException(ResourseNotFoundException ex) {
    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage());
    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
  }
  
  @ExceptionHandler(DatabaseException.class)
  public ResponseEntity<Object> databaseEXception(DatabaseException ex){
  ErrorDetails errorDetails=new ErrorDetails(new Date(), ex.getMessage());
  return new ResponseEntity<>(errorDetails,HttpStatus.EXPECTATION_FAILED);
  }
  
  @ExceptionHandler(NonTransientException.class)
  public ResponseEntity<Object> nonTransientEXception(NonTransientException ex){
  ErrorDetails errorDetails=new ErrorDetails(new Date(), ex.getMessage());
  return new ResponseEntity<>(errorDetails,HttpStatus.EXPECTATION_FAILED);
  }
  
  @ExceptionHandler(TransientException.class)
  public ResponseEntity<Object> transientEXception(TransientException ex){
  ErrorDetails errorDetails=new ErrorDetails(new Date(), ex.getMessage());
  return new ResponseEntity<>(errorDetails,HttpStatus.EXPECTATION_FAILED);
  }


}
