package com.mindstix.userapplication.exception;

import org.springframework.dao.NonTransientDataAccessException;

public class NonTransientException extends NonTransientDataAccessException {
  public NonTransientException(String exception)
  {
    super(exception);
  }

}
