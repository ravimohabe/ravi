package com.mindstix.userapplication.exception;

import java.sql.SQLException;
import org.springframework.dao.DataAccessException;

public class DatabaseException extends DataAccessException {
  String exception;

  public DatabaseException(String exception) {
    super(exception);
  }
}
