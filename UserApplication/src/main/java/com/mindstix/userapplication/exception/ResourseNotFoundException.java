package com.mindstix.userapplication.exception;

public class ResourseNotFoundException extends RuntimeException{
  
  public ResourseNotFoundException(String exception) {
    super(exception);
  }
}
