package com.mindstix.userapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mindstix.userapplication.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>{

  User findByUserid(Integer id);
}
  