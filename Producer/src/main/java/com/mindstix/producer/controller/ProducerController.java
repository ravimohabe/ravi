package com.mindstix.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mindstix.producer.rmq.*;

import com.mindstix.producer.rmq.Producer;

@RestController
public class ProducerController {

	@Autowired
	private Producer producer;

	@RequestMapping("/sendevent/{event}")
	public String sendEvent(@PathVariable String event) {

		producer.producemsg(event);
		return "done";
	}

}
