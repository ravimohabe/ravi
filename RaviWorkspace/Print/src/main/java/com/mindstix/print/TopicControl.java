package com.mindstix.print;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicControl {
	
	@Autowired	
	private TopicService topicservice;
	
	@RequestMapping("/topic")
	public List<Topic> getAllTopics()
	{
       return topicservice.getAllTopics();
	}
	
	@RequestMapping("/hi")
	public String ravi()
	{
       return "ravi";
	}
	
}
