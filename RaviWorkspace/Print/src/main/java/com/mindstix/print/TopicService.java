package com.mindstix.print;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
     private List<Topic> topics= Arrays.asList(
			new Topic("Spring","SpringFramework","Framework Discription"),
			new Topic("java","core java","core java Discription"),
			new Topic("javascript","tomcat","server")
			);	
	
     public List<Topic> getAllTopics()
     {
    	 return topics; 
     }
	

}
