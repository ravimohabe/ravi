package com.mindstix.bankapplicationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroDemoApplication.class, args);
	}

}
