package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicControl {
	
	
	@RequestMapping("/hello")
	
	public List<Topic> sayHi()
	{
		
		return Arrays.asList(
				new Topic("Spring","SpringFramework","Framework Discription"),
				new Topic("java","core java","core java Discription"),
				new Topic("javascript","tomcat","server")
				);	
	}
	
}
