package Assignments;

public class User {

  private String name;
  private String add;
  private Integer age;
  
  public User(String name, String add, Integer age) {
    super();
    this.name = name;
    this.add = add;
    this.age = age;
  }

  @Override
  public String toString() {
    return "User [name=" + name + ", add=" + add + ", age=" + age + "]";
  }
 
}