/*
 * Write a Java program to find the second largest and second smallest element in an array.
 */
package Assignments;

import java.util.Scanner;

public class FIndSecondLarge {

  public static void main(String str[]) {
    Scanner input = new Scanner(System.in);


    System.out.print("Enter sizeof your array");
    int s = input.nextInt();

    if (s < 2)
      System.out.println(" Invalid Input ");

    System.out.print("Enter your Array element");

    int Arr[] = new int[20];

    for (int i = 0; i < s; i++) {
      Arr[i] = input.nextInt();
    }

    int first, second;

    first = second = Integer.MAX_VALUE;

    for (int i = 0; i < s; i++) {
      /*
       * If current element is smaller than first then update both first and second
       */
      if (Arr[i] < first) {
        second = first;
        first = Arr[i];
      }


      else if (Arr[i] < second && Arr[i] != first)
        second = Arr[i];
    }
    if (second == Integer.MAX_VALUE)
      System.out.println("There is no second smallest element");
    else
      System.out.println(
          "The smallest element is " + first + " and second Smallest element is " + second);
    int large, secondlarge;

    large = secondlarge = Integer.MIN_VALUE;

    for (int i = 0; i < s; i++) {
      /*
       * If current element is smaller than first then update both first and second
       */
      if (Arr[i] > large) {
        secondlarge = large;
        large = Arr[i];
      }


      else if (Arr[i] > secondlarge && Arr[i] != large)
        secondlarge = Arr[i];
    }
    if (second == Integer.MIN_VALUE)
      System.out.println("There is no second LARGEST element");
    else
      System.out.println(
          "The largest element is " + large + " and second largest element is " + secondlarge);
  }

}
