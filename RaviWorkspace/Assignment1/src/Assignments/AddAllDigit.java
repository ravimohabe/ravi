package Assignments;

import java.util.Scanner;


public class AddAllDigit {

  public static void main(String Str[]) {

    Scanner input = new Scanner(System.in);

    System.out.println("Enter the integer between 0 to 1000");

    int num = input.nextInt();
    int digit, sum = 0;

    while (num > 9) {
      digit = num % 10;
      sum = sum + digit;
      num = num / 10;

    }
    sum = sum + num;

    System.out.println("Sum of all digit of number is " + sum);



  }


}
