package Assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CollectionProgram {

  public static void main(String args[]) {

    ArrayList<Integer> al = new ArrayList<Integer>();

    System.out.print("How many record you want to enter ");
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int temp;

    for (int i = 0; i < n; i++) {
      al.add(sc.nextInt());
    }

    System.out.println("your element: " + al);

    Collections.sort(al);


    System.out.println("your element in ascending order : " + al);

    Collections.reverse(al);

    System.out.println("your element in descending order : " + al);

    Map<Integer, Integer> hm = new HashMap<Integer, Integer>();

    for (int i : al) {
      Integer j = hm.get(i);
      hm.put(i, (j == null) ? 1 : j + 1);
    }

    System.out.println("Occurance of each element in java is given below : ");

    for (Map.Entry<Integer, Integer> val : hm.entrySet()) {
      System.out
          .println("Element " + val.getKey() + " " + "occurs" + ": " + val.getValue() + " times");
    }

    System.out.print("Enter your number you want to search in collection: ");

    int s = sc.nextInt();
    int index = -1;
    int flag = 0;

    for (int k : al) {
      index++;

      if (s == k)
        flag = 1;
      break;
    }

    if (flag == 1)
      System.out.print("Your number found at index no: " + index);

    else
      System.out.println("your Element does no found");


    System.out.println("Enter your Collection of number to append it in exiting collectiion");

    System.out.println("Enter size of another collection element:");

    int a = sc.nextInt();

    for (int i = 0; i < a; i++) {
      al.add(sc.nextInt());
    }


    System.out.println("your Collection element after appending are: " + al);

    Collections.sort(al);

    System.out.println("Your Collection element after sorting are: " + al);


  }


}
