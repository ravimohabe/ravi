package Assignments;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ListSet {

  public static void main(String arg[]) {
    Set<String> st = new HashSet<String>();

    Scanner sc = new Scanner(System.in);


    int z = 0;
    System.out.println("This is collection framework  program: ");
    st.add("Ravi");
    st.add("kaivalya");
    st.add("Aditya");
    st.add("parag");
    st.add("rahul");

    Map<Integer, String> mp = new HashMap<Integer, String>();

    int i = 0;

    for (String temp : st) {
      mp.put(i, temp);
      i++;
    }

    System.out.println(mp);

    System.out.println(
        "1: Listing all key value pair \n 2: Delete the kay value pair \n  3:Search the key value pair \n 4: Check for Empty  ");
    System.out.println("please enter your choice");
    int ch = sc.nextInt();
    switch (ch) {

      case 1:
        System.out.println(mp);
        break;

      case 2:

        System.out.println("Enter the value to delete");
        String delstr = sc.nextLine();

        int j = 0;

        for (Map.Entry<Integer, String> entry : mp.entrySet()) {
          if (delstr.equals(entry.getValue())) {
            j = entry.getKey();
            break;
          }
        }

        mp.remove(j);

        break;

      case 3:
        System.out.println("Enter value to be search: ");
        String s = sc.nextLine();
        if (mp.containsValue(s) == true) {
          System.out.println("hash table do have " + s + " as value");
        } else {
          System.out.println("hash table do not have " + s + " as value");
        }

        break;

      case 4:

        if (mp.isEmpty() == true) {
          System.out.println("hash table is Empty");
        } else {
          System.out.println("hash table is not Empty");
        }

        break;

      default:

        System.out.println("Please Enter your correct choice");

    }

  }

}
