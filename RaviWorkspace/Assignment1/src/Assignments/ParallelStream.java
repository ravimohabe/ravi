package Assignments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ParallelStream {

  

  public static void main(String str[])
  {
  
    List<User> list=new ArrayList<>();
    
    list.add(new User("Ravi","baner",25));
    list.add(new User("Aditya","alandi",25));
    list.add(new User("prashant","pune",25));
    list.add(new User("Ravi","pune",25));
    list.add(new User("Ravi","alandi",25));
    list.add(new User("kaivalya","kothrud",25));
    
    Stream<User> parallelstream=list.parallelStream();
    
    parallelstream.forEach(s->doprocess(s));
    
  }

  private static void doprocess(User s) { 
    System.out.print(s.toString());
  }
}
