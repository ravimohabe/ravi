package Assignments;


// Program of converting string to date time


import java.util.Scanner;
import java.util.TimeZone;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ConvertStringToDate {

  public static void main(String arg[]) {
    System.out.print("Enter your date and time (yyyy-MM-dd HH:mm:ss)");
    Scanner sc = new Scanner(System.in);
    String str = sc.nextLine();


    SimpleDateFormat forment = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
    forment.setTimeZone(TimeZone.getTimeZone("UTC"));
    Date d1 = null;

    try {
      d1 = forment.parse(str);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println(d1);
  }
}
