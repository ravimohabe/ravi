package Assignments;

import java.util.Scanner;
import java.util.regex.*;



public class PasswordCheckWithRegex {

  public static void main(String arg[]) {
    System.out.println("enter your password String");
    Scanner sc = new Scanner(System.in);

    String pass = sc.nextLine();


    int symbol = 0, digit = 0, letter = 0;

    Pattern p = Pattern.compile("[a-zA-Z0-9!@#$?]");
    Matcher m = p.matcher(pass);

    boolean b = m.matches();

    if (b)
      System.out.println("your String is valid");
    else
      System.out.println("your string does not match");

  }

}
