package Assignments;

import java.util.Scanner;

public class PrintIndivisualDigit {

	public static void main(String str[]) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter your integer");

		int num = input.nextInt();

		int digit, temp, count = 1;

		temp=num;
		
		while (num > 9) {

			digit = num % 10;
			num = num / 10;

			count = count * 10;

		}

		num=temp;
		
		while (count >= 10)

		{

			digit = num / count;

			num = num % count;
			
			count=count/10;
			
			System.out.print(" "+digit);

		}
		
		System.out.print(" "+ num);

	}

}
