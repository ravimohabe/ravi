package com.mindstix.zuulapigateway.filter;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class PreFilter extends ZuulFilter   {
  private static Logger LOG = LoggerFactory.getLogger(PreFilter.class);
  @Override
  public boolean shouldFilter() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public Object run() throws ZuulException {
    RequestContext ctx= RequestContext.getCurrentContext();
    HttpServletRequest request=ctx.getRequest();
    LOG.info("PreFilter: "+ String.format("%s request to %s",request.getMethod(), request.getRequestURI().toString()));
    LOG.info("enter into prefilter ");
    return null;
  }

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 1;
  }

}
