package com.mindstix.commentapplication.service;

import java.util.List;
import com.mindstix.commentapplication.model.Comment;

/**
 * 
 * @author ravimohabe
 *
 */
public interface CommentService {

  /**
   * This function is returns list of all comment
   * 
   * @return It return list of comment
   */
  List<Comment> getComment();

  /**
   * this function is for adding comment
   * 
   * @param comment
   */
  void addComment(Comment comment);

  /**
   * this function return list of comment by given user id
   * 
   * @param id user id
   * @return list of comment
   */
  List<Comment> getCommentByUserId(Integer id);

  /**
   * this is used for deleting comment by comment id
   * 
   * @param id comment id
   * @return return deleted comment
   */

  Comment deleteCommentByCommentId(Integer id);

}
