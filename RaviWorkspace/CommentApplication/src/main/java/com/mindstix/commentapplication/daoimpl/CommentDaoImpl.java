package com.mindstix.commentapplication.daoimpl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import com.mindstix.commentapplication.dao.CommentDao;
import com.mindstix.commentapplication.exception.DatabaseException;
import com.mindstix.commentapplication.exception.NonTransientException;
import com.mindstix.commentapplication.exception.ResourseNotFoundException;
import com.mindstix.commentapplication.exception.TransientException;
import com.mindstix.commentapplication.model.Comment;
import com.mindstix.commentapplication.repository.CommentRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;

@Controller
public class CommentDaoImpl implements CommentDao {

  final private static Logger LOGGER = LoggerFactory.getLogger(CommentDaoImpl.class);
  @Autowired
  CommentRepository commentRepository;

  /**
   * This function id used to get list of all comment from mysql database
   * 
   * @return list of comment
   */
  // @HystrixCommand(ignoreExceptions = {TransientException.class})

  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {DatabaseException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public List<Comment> findAll() {
    LOGGER.info("fetching list of comment from database");

    try {
      List<Comment> comment = commentRepository.findAll();
      return comment;
    } catch (TransientException ex) {
      throw new TransientException("not able to get all comment ");
    } catch (NonTransientException ex) {
      throw new NonTransientException("Not able to get all comment");
    }
  }

  /**
   * This function is used to save comment
   * 
   * @param comment
   */
  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {TransientException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public void save(Comment comment) {
    LOGGER.info("saving comment to database");
    try {
      commentRepository.save(comment);
    } catch (TransientException ex) {
      throw new NonTransientException("Not able to save in database");
    } catch (NonTransientException e) {
      throw new NonTransientException("not able to save in database");
    }
  }

  /**
   * This function is used to delete comment
   * 
   * @param comment to delete
   */
  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {DatabaseException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public void deleteComment(Comment newComment) {
    LOGGER.info("deleting comment from database");
    try {
      commentRepository.delete(newComment);
    } catch (TransientException e) {
      throw new TransientException("Not able to delete");
    } catch (NonTransientException e) {
      throw new NonTransientException("Not able to delete");
    }
  }

  @HystrixCommand(ignoreExceptions = {TransientException.class})
  @Retryable(value = {TransientException.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
  @Override
  public List<Comment> findAllCommentByUserid(Integer id) {

    List<Comment> comment = null;
    try {
      comment = commentRepository.findByUserid(id);

    } catch (NonTransientException e) {
      throw new NonTransientException("No coomment present in database with userid=" + id);
    } catch (TransientException ex) {
      throw new TransientException("No comment present in database with userid=" + id);
    }
    return comment;
  }
}
