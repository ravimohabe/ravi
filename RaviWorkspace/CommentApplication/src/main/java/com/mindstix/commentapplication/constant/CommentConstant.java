package com.mindstix.commentapplication.constant;

public class CommentConstant {
  public final static String COMMENT = "/comment";
  public final static String COMMENTBYUSEID = "/comment/user/{id}";
  public final static String COMMENTBYUSE = "/comment/user";
  public final static String COMMENTBYCOMMENTID = "/comment/user/{id}";
}
   