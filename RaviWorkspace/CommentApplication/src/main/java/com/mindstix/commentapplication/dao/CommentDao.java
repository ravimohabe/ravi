package com.mindstix.commentapplication.dao;

import java.util.List;
import com.mindstix.commentapplication.model.Comment;

public interface CommentDao {

  List<Comment> findAll();

  void save(Comment comment);

  void deleteComment(Comment newComment);

  List<Comment> findAllCommentByUserid(Integer id);

}
