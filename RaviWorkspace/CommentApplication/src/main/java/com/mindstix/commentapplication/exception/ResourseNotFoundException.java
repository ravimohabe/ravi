package com.mindstix.commentapplication.exception;

public class ResourseNotFoundException extends RuntimeException {
  
  public ResourseNotFoundException(String exception)
  {
    super(exception);
  }
}
