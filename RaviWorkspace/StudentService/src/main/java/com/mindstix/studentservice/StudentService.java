package com.mindstix.studentservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class StudentService {

	private List<Student> student = new ArrayList<>(Arrays.asList(new Student("001", "Ravi", "Computer"),
			new Student("002", "Rahul", "Mech"), new Student("003", "pradip", "Chemical")));

	public List<Student> getAllStudent() {
		return student;
	}

	public Student getStudent(String id) {
		return student.stream().filter(t -> t.getId().equals(id)).findFirst().get();

	}

	public void addstudent(Student student2) {

		student.add(student2);

	}

	public void updatestudent(String id, Student student2) {

		for (int i = 0; i < student.size(); i++) {
			Student t = student.get(i);
			if (t.getId().equals(id)) {
				student.set(i, student2);
				return;

			}

		}

	}

	public void deleteStudent(String id) {
		student.removeIf(t -> t.getId().equals(id));

	}

}
