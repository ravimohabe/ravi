package com.mindstix.studentservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentControl {

  @Autowired
  private StudentService studentservice;

  @RequestMapping("/student")
  public List<Student> getAllStudent() {
    return studentservice.getAllStudent();
  }

  @RequestMapping("/hi")
  public String getreply() {
    return "Hello Ravi";
  }

  @RequestMapping("/student/{id}")
  public Student getstudent(@PathVariable String id) {
    return studentservice.getStudent(id);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/student")
  public void addstudent(@RequestBody Student student) {
    studentservice.addstudent(student);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/student/{id}")
  public void updatestudent(@RequestBody Student student, @PathVariable String id) {
    studentservice.updatestudent(id, student);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/student/{id}")
  public void deleteStudent(@PathVariable String id) {
    studentservice.deleteStudent(id);
  }



}
