package com.mindstix.bankapplicationapi.daoimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import com.mindstix.bankapplicationapi.dao.CustomerDao;
import com.mindstix.bankapplicationapi.model.Customer;
import com.mindstix.bankapplicationapi.repository.CustomerRepository;

@Controller
public class CustomerDaoImpl implements CustomerDao {

  @Autowired
  private CustomerRepository customerRepository;

  @Override
  public List<Customer> findAll() {
    List<Customer> customer = customerRepository.findAll();
    return customer;
  }

  @Override
  public Customer findCustomerById(int id) {

    Customer customer = customerRepository.findByCustomerId(id);
    return customer;
  }

  @Override
  public Customer save(Customer customer) {

    Customer newcustomer = customerRepository.save(customer);
    return newcustomer;
  }

  @Override
  public Customer findByCustomerId(int id) {

    Customer customer = customerRepository.findByCustomerId(id);
    return customer;
  }

  @Override
  public void delete(Customer customerToDelete) {
    customerRepository.delete(customerToDelete);
  }
}
