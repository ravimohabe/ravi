package com.mindstix.bankapplicationapi.service;

import java.util.List;
import com.mindstix.bankapplicationapi.model.Account;

public interface AccountService {
  List<Account> getAllAccount();

  Account deleteAccount(int accNo);

  // List<Account> getAccount(int id);



}
