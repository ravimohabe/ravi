package com.mindstix.bankapplicationapi.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mindstix.bankapplicationapi.constant.BankAppConstant;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.service.AccountService;

@RestController
@RequestMapping(BankAppConstant.BANKURL)
public class AccountController {

  @Autowired
  private AccountService accountService;

  @RequestMapping(BankAppConstant.ACCOUNT)
  List<Account> getAllAccount() {
    List<Account> account = accountService.getAllAccount();
    return account;
  }

  @RequestMapping(method = RequestMethod.DELETE, value = BankAppConstant.ACCOUNTDETAIL)
  public Account deleteAccount(@PathVariable int id, @PathVariable int accNo) {
    Account account = accountService.deleteAccount(accNo);

    return account;
  }
}
