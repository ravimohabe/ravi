package com.mindstix.bankapplicationapi.serviceimpl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.mindstix.bankapplicationapi.exeption.ResourseNotFoundException;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.repository.AccountRepository;
import com.mindstix.bankapplicationapi.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {
  @Autowired
  AccountRepository accountRepository;
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

  @Override
  public List<Account> getAllAccount() {

    List<Account> data = accountRepository.findAll();
    if (CollectionUtils.isEmpty(data)) {
      LOGGER.error("Database is empty");
      throw new ResourseNotFoundException("Database is Empty");
    }

    else {
      LOGGER.info("Data Retrived from database");
      return data;
    }
  }

  @Override
  public Account deleteAccount(int accNo) {
    List<Account> account = accountRepository.findAll();
    Account newaccount = newaccount =
        account.stream().filter(a -> a.getAccountNumber().equals(accNo)).findFirst().get();


    accountRepository.delete(newaccount);
    return newaccount;
  }

}
