  package com.mindstix.bankapplicationapi.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.mindstix.bankapplicationapi.constant.BankAppConstant;
import com.mindstix.bankapplicationapi.exeption.ResourseNotFoundException;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.model.Customer;
import com.mindstix.bankapplicationapi.service.CustomerService;


@RestController
@RequestMapping(BankAppConstant.BANKURL)
public class CustomerController {

  final private static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
  @Autowired
  private CustomerService customerService;



  @RequestMapping(BankAppConstant.CUSTOMER)
  public List<Customer> getAllCustomer() {
    LOGGER.info("Request received to get all customer");
    List<Customer> data = customerService.getAllCustomer();
    return data;
  }

  @RequestMapping(BankAppConstant.CUSTOMER_ID)
  public Customer getCustomer(@PathVariable int id) {
    LOGGER.info("Requet received to show customer with customer with customer id={}", id);
    Customer customer = customerService.getCustomer(id);
    return customer;
  }

  @RequestMapping(method = RequestMethod.POST, value = BankAppConstant.CUSTOMER)
  @ResponseStatus(value = HttpStatus.CREATED)
  public Customer addCustomer(@RequestBody Customer customer) {
    LOGGER.info("Adding customer to database");
    Customer newcustomer = customerService.addCustomer(customer);
    return newcustomer;
  }


  @RequestMapping(method = RequestMethod.DELETE, value = BankAppConstant.CUSTOMER_ID)
  public Customer deleteCustomer(@PathVariable int id) {
    LOGGER.info("Requet received to delete customer with customer with customer id={}", id);
    Customer customer = customerService.deleteCustomer(id);
    return customer;
  }

  @RequestMapping(method = RequestMethod.PUT, value = BankAppConstant.CUSTOMER_ID)
  public Customer updateCustomer(@PathVariable int id, @RequestBody Customer customer) {
    LOGGER.info("Requet received to update customer with customer with customer id={}", id);
    Customer newcustomer = customerService.updateCustomer(id, customer);
    return newcustomer;
  }

  @RequestMapping(BankAppConstant.ALLACCOUNT)
  public List<Account> getAllAccountByCustomerId(@PathVariable int id) {
    List<Account> accounts = customerService.getAllAccountByCustomerId(id);
    return accounts;
  }

  @RequestMapping(BankAppConstant.ACCOUNTDETAIL)
  public Account getAccountByCustomerId(@PathVariable int id, @PathVariable int accNo) {
    Account account = customerService.getAccountByCustomerId(id, accNo);
    return account;
  }

  @RequestMapping(method = RequestMethod.POST, value = BankAppConstant.ALLACCOUNT)
  public Account addAccountByCustomerId(@PathVariable int id, @RequestBody Account account) {
    Account createdAccount = customerService.addAccountToCustomer(id, account);


    return createdAccount;
  }



}
