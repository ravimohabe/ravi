package com.mindstix.bankapplicationapi.serviceimpl;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.mindstix.bankapplicationapi.dao.CustomerDao;
import com.mindstix.bankapplicationapi.exeption.ResourseNotFoundException;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.model.Customer;
import com.mindstix.bankapplicationapi.repository.CustomerRepository;
import com.mindstix.bankapplicationapi.service.CustomerService;
import net.bytebuddy.dynamic.DynamicType.Builder.FieldDefinition.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

  @Autowired
  private CustomerDao customerDao;

  @Override
  public List<Customer> getAllCustomer() {
    List<Customer> data = customerDao.findAll();
    if (CollectionUtils.isEmpty(data)) {
      LOGGER.error("Database is empty");
      throw new ResourseNotFoundException("Database is Empty");
    }

    else {
      LOGGER.info("Data Retrived from database");
      return data;
    }
  }

  @Override
  public Customer getCustomer(int id) {

    Customer customer = customerDao.findByCustomerId(id);

    if (customer == null) {
      LOGGER.error("No customer found with  Customer id: " + id);
      throw new ResourseNotFoundException("No customer found with  Customer id: " + id);

    } else {
      LOGGER.info("Customer find with customer id={}", id);
      return customer;

    }
  }

  @Override
  public Customer addCustomer(Customer customer) {
    Customer newcustomer = customerDao.save(customer);
    return newcustomer;
  }

  @Override
  public Customer deleteCustomer(int id) {
    Customer customerToDelete = null;
    try {
      customerToDelete = customerDao.findByCustomerId(id);
      customerDao.delete(customerToDelete);
      LOGGER.info("Customer with given id={} deleted", id);
    } catch (Exception e) {
      LOGGER.error("  Customer with given Id is not Available");
      throw new ResourseNotFoundException("No customer found with  Customer id: " + id);
    }
    return customerToDelete;
  }

  public Customer updateCustomer(int id, Customer customer) {
    Customer newcustomer = customerDao.findByCustomerId(id);
    Customer customer1;
    if (newcustomer == null) {
      LOGGER.error("No customer found with  Customer id: " + id);
      throw new ResourseNotFoundException("No customer found with  Customer id: " + id);
    } else {
      LOGGER.info("Customer with Customer id={} updated successfully " + id);
      newcustomer.setCustomerName(customer.getCustomerName());
      customer1 = customerDao.save(newcustomer);
      return customer1;
    }
  }

  @Override
  public List<Account> getAllAccountByCustomerId(int id) {

    Customer newcustomer = customerDao.findByCustomerId(id);
    List<Account> account;
    if (newcustomer == null) {
      LOGGER.error("No customer found with  Customer id: " + id);
      throw new ResourseNotFoundException("No customer found with  Customer id: " + id);
    } else {
      LOGGER.info("Customer with Customer id={} found" + id);
      account = newcustomer.getAccount();
    }
    return account;
  }

  @Override
  public Account getAccountByCustomerId(int id, int accNo) {
    List<Account> account = this.getAllAccountByCustomerId(id);
    Account newaccount;
    newaccount = account.stream().filter(a -> a.getAccountNumber().equals(accNo)).findFirst().get();
    return newaccount;
  }

  @Override
  public Account addAccountToCustomer(int id, Account account) {
    Customer newcustomer = customerDao.findByCustomerId(id);

    Customer customer1;
    if (newcustomer == null) {
      LOGGER.error("No customer found with  Customer id: " + id);
      throw new ResourseNotFoundException("No customer found with  Customer id: " + id);
    } else {
      LOGGER.info("Account with AccountId={} created for Customer with Customer id={} " + id
          + account.getAccountNumber());

      newcustomer.addAccount(account);
      customer1 = customerDao.save(newcustomer);
      return account;
    }
  }

}
