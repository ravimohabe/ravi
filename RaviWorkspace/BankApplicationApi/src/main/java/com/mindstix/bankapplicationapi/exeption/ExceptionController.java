    package com.mindstix.bankapplicationapi.exeption;

import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> globalExeption(Exception ex) {
    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage());
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourseNotFoundException.class)
  public ResponseEntity<Object> resourseNotFoundException(ResourseNotFoundException ex) {
    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage());
    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
  }
}
