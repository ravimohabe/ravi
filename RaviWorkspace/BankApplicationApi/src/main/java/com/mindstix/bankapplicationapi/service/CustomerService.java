package com.mindstix.bankapplicationapi.service;

import java.util.List;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.model.Customer;

public interface CustomerService {

  /**
   * 
   * @return
   */
  List<Customer> getAllCustomer();
  Customer getCustomer(int id);
  Customer addCustomer(Customer customer);
  Customer deleteCustomer(int id);
  Customer updateCustomer(int id, Customer customer);
  List<Account> getAllAccountByCustomerId(int id);
  Account getAccountByCustomerId(int id, int accNo);
  Account addAccountToCustomer(int id, Account account);

}
