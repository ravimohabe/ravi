package com.mindstix.bankapplicationapi;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.mindstix.bankapplicationapi.model.Account;
import com.mindstix.bankapplicationapi.model.Customer;
import com.mindstix.bankapplicationapi.repository.CustomerRepository;

@SpringBootApplication
public class BankApplicationApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(BankApplicationApiApplication.class, args);
	}
	
}
