package com.mindstix.bankapplicationapi.dao;

import java.util.List;
import com.mindstix.bankapplicationapi.model.Customer;

public interface CustomerDao {

  List<Customer> findAll();

  Customer findCustomerById(int id);

  Customer save(Customer customer);

  Customer findByCustomerId(int id);

  void delete(Customer customerToDelete);

}
