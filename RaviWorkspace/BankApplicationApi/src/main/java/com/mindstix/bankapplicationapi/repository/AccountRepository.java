package com.mindstix.bankapplicationapi.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mindstix.bankapplicationapi.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

  

}
