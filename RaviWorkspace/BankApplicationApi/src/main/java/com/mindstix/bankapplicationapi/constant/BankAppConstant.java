package com.mindstix.bankapplicationapi.constant;

public class BankAppConstant {
  public final static String BANKURL = "/bank";
  public final static String CUSTOMER = "/customer";
  public final static String CUSTOMER_ID = "/customer/{id}";
  public final static String ACCOUNT = "/account";
  public final static String ALLACCOUNT = "/customer/{id}/account";
  public final static String ACCOUNTDETAIL = "/customer/{id}/account/{accNo}";
}
