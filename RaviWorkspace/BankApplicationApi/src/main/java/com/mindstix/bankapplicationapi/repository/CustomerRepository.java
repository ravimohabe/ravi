package com.mindstix.bankapplicationapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mindstix.bankapplicationapi.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

  Customer findByCustomerId(Integer id);
}
