package com.mindstix.bankapplicationapi.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int customerId;
  private String customerName;
  private String address;

  // @OneToMany(cascade = CascadeType.ALL)
  @OneToMany(cascade = CascadeType.REMOVE)
  private List<Account> account;

  public void setAccount(List<Account> account) {
    this.account = account;
  }

  public List<Account> getAccount() {
    return account;
  }

  public Customer() {}


  public Customer(int customerId, String customerName, String address) {
    super();
    this.customerId = customerId;
    this.customerName = customerName;
    this.address = address;
    // this.accounts = accounts;
  }

  public int getCustomerId() {
    return customerId;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Account addAccount(Account newAccount) {
    this.account.add(newAccount);
    return newAccount;
  }

  public void deleteAccount(Account account2) {
    this.account.remove(account2);

  }
}

