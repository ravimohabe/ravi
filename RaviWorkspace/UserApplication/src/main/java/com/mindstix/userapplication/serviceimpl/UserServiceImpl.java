package com.mindstix.userapplication.serviceimpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import com.mindstix.userapplication.dao.UserDao;
import com.mindstix.userapplication.exception.DatabaseException;
import com.mindstix.userapplication.exception.GlobalException;
import com.mindstix.userapplication.exception.ResourseNotFoundException;
import com.mindstix.userapplication.feignclientinterface.MyFeignClient;
import com.mindstix.userapplication.model.Comment;
import com.mindstix.userapplication.model.User;
import com.mindstix.userapplication.service.UserService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class UserServiceImpl implements UserService {
  final private static Logger LOGGER = LoggerFactory.getLogger(UserService.class);
  @Autowired
  private UserDao userDao;

  @Autowired
  MyFeignClient feignClient;

  /**
   * This function is used to get all user
   * 
   * @return return list of user
   */
  @Override
  public List<User> getUser() {
    LOGGER.info("Contol came in getUser funtion");
    List<User> users = userDao.findAll();
    if (users == null) {
      LOGGER.info("NO User is present in database");
      throw new ResourseNotFoundException("DataBase is empty");
    } else
      return users;
  }

  /**
   * This function is used to add user
   * 
   * @param user
   * @throws SQLException
   */
  @Override
  public void addUser(User user) throws SQLException {
    LOGGER.info("Contol came in addUser() function");
    userDao.save(user);
  }

  /**
   * This function is used to get all comment by user id
   * 
   * @param id
   * @return list of comment by given user id
   */
//  @HystrixCommand(fallbackMethod = "getFallBackComment", groupKey = "commentservice",
//      commandKey = "getCommentByUserId")
  @Override
  public List<Comment> getCommentByUserId(int id) {
    LOGGER.info("Contol came in getCommentByUserId() function");
    List<Comment> comment = null;
    try {
      comment = feignClient.findById(id);
    } catch (RuntimeException e) {
      throw new ResourseNotFoundException("No Comment present with user id=" + id);
    }

    if (comment.isEmpty()) {
      LOGGER.info("No Comment present with user id={}", id);
      throw new ResourseNotFoundException("No Comment present with user id=" + id);
    } else {
      return comment;
    }
  }

  /**
   * 
   * @param id user id
   * @return
   */
//  public List<Comment> getFallBackComment(int id) {
//    throw new ResourseNotFoundException("Sorry Comment service not working");
//  }

  /**
   * This function is used add comment
   * 
   * @param comment
   */
  @Override
  public void addComment(Comment comment) {
    LOGGER.info("Contol came in addComment() function");
    feignClient.addComment(comment);
  }

  /**
   * This function is used to get user by given id
   * 
   * @param id user id
   * @return it return user of given id
   */
  @Override
  public User getUserById(int id) {
    LOGGER.info("Contol came in getUserById() function");
    User user = userDao.findById(id);
    if (user == null) {
      LOGGER.info("No User present with userid={}", id);
      throw new ResourseNotFoundException("No user present with user id=" + id);
    } else
      return user;
  }

  /**
   * This function is used to delete comment
   * 
   * @param id comment id
   */
  @Override
  public void deleteComment(Integer id) {
    LOGGER.info("Contol came in deleteCommment() function");
    feignClient.deleteComment(id);
  }
}
