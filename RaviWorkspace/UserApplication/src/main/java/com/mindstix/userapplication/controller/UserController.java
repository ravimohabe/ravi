package com.mindstix.userapplication.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mindstix.userapplication.constant.UserConstant;
import com.mindstix.userapplication.model.Comment;
import com.mindstix.userapplication.model.CommentList;
import com.mindstix.userapplication.model.User;
import com.mindstix.userapplication.service.UserService;

@RefreshScope
@RestController
@RequestMapping(UserConstant.USERURL)
public class UserController {

  final private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
  @Autowired
  private UserService userService;

  /**
   * This API is used to fetch list of all users
   * 
   * @return it return list of all users
   */

  
  @RequestMapping(method = RequestMethod.GET)
  public List<User> getUser() {
    LOGGER.info("Request received to get all User");
    List<User> userList = userService.getUser();
    return userList;
  }

  /**
   * This API is used to fetch details user of given id
   * 
   * @param id
   * @return returns user details
   */
  @RequestMapping(value = UserConstant.USERID, method = RequestMethod.GET)
  public User getUserById(@PathVariable int id) {
    LOGGER.info("Request received to get user info of userid={}", id);
    User userList = userService.getUserById(id);
    return userList;
  }

  /**
   * This API iS used to add USer
   * 
   * @param user
   * 
   * @author ravimohabe
   * @throws SQLException 
   */

  @RequestMapping(method = RequestMethod.POST)
  public void addUser(@RequestBody User user) throws SQLException {
    LOGGER.info("Request received to add user");
    userService.addUser(user);
  }

  /**
   * This API is used to fetch all comment by given user id
   * 
   * @param id
   * @return
   */
  @RequestMapping(value = UserConstant.COMMENTBYUSERID, method = RequestMethod.GET)
  public List<Comment> getCommentByUserId(@PathVariable int id) {
    LOGGER.info("Request received to get all comment by user id={}", id);
    List<Comment> comments = userService.getCommentByUserId(id);
    return comments;
  }

  /**
   * This API is to add comment
   * 
   * @param comment
   * @author ravimohabe
   */

  @RequestMapping(value = UserConstant.COMMENT, method = RequestMethod.POST)
  public void addComment(@RequestBody Comment comment) {
    LOGGER.info("Request received to add comment");
    userService.addComment(comment);
  }

  /**
   * This API is to delete comment by of given comment id
   * 
   * @param id comment id
   */

  @RequestMapping(value = UserConstant.COMMENTID, method = RequestMethod.DELETE)
  public void deleteComment(@PathVariable Integer id) {
    LOGGER.info("Request received to delete comment by comment id={}", id);
    userService.deleteComment(id);
  }
}
