package com.mindstix.userapplication.feignclientinterface;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mindstix.userapplication.constant.UserConstant;
import com.mindstix.userapplication.model.Comment;

@FeignClient(name = "comment-service")
public interface MyFeignClient {
  @RequestMapping(value = UserConstant.COMMENTBYID)
  public List<Comment> findById(@PathVariable(value = "id") int id);

  @RequestMapping(value = UserConstant.USERCOMMENT, method = RequestMethod.POST)
  public void addComment(@RequestBody Comment comment);

  @RequestMapping(value=UserConstant.COMMENTBYID, method=RequestMethod.DELETE)
  public void deleteComment(@PathVariable(value = "id") Integer id);
}
