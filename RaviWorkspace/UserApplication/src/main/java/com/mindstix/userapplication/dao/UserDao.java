package com.mindstix.userapplication.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import com.mindstix.userapplication.model.User;

public interface UserDao {

  List<User> findAll();

  void save(User user) throws SQLException;

 User findById(int id);

}
