package com.mindstix.userapplication.exception;

import org.springframework.dao.TransientDataAccessException;

public class TransientException extends TransientDataAccessException {
  
  public TransientException(String exception)
  {
    super(exception);
  }

}
