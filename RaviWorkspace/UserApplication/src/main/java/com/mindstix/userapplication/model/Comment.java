package com.mindstix.userapplication.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Comment {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int commentid;
  private int userid;
  private String commentByUser;
  private String rating;

  public Comment() {
    super();
  }

  public Comment(int commentid, int userid, String commentByUser, String rating) {
    super();
    this.commentid = commentid;
    this.userid = userid;
    this.commentByUser = commentByUser;
    this.rating = rating;
  }
  
  public int getCommentid() {
    return commentid;
  }
  public void setCommentid(int commentid) {
    this.commentid = commentid;
  }
  public int getUserid() {
    return userid;
  }
  public void setUserid(int userid) {
    this.userid = userid;
  }
  public String getCommentByUser() {
    return commentByUser;
  }
  public void setCommentByUser(String commentByUser) {
    this.commentByUser = commentByUser;
  }
  public String getRating() {
    return rating;
  }
  public void setRating(String rating) {
    this.rating = rating;
  }

}
