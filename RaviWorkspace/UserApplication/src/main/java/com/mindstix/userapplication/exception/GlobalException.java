package com.mindstix.userapplication.exception;

public class GlobalException extends Exception {

  public GlobalException(String exception) {
    super(exception);
  }
}
