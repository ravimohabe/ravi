package com.mindstix.statemachine.serviceimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;

import com.mindstix.statemachine.configstatemachine.PaymentEvent;
import com.mindstix.statemachine.configstatemachine.PaymentState;
import com.mindstix.statemachine.configstatemachine.PaymentStateChangeInterceptor;
import com.mindstix.statemachine.model.Payment;
import com.mindstix.statemachine.repository.PaymentRepository;
import com.mindstix.statemachine.service.PaymentService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PaymentServiceImpl implements PaymentService {

	public static final String PAYMENT_ID_HEADER = "payment_id";
	@Autowired
	private PaymentRepository paymentRepository;
	private StateMachineFactory<PaymentState, PaymentEvent> stateMachineFactory;
	private PaymentStateChangeInterceptor paymentStateChangeIntercepter;

	@Override
	public Payment newPayment(Payment payment) {
		payment.setState(PaymentState.NEW);
		return paymentRepository.save(payment);
	}

	@Transactional
	@Override
	public StateMachine<PaymentState, PaymentEvent> preAuth(Long paymentId) {
		StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);

		sendEvent(paymentId, sm, PaymentEvent.PRE_AUTHORISE);

		return null;
	}

	@Transactional
	@Override
	public StateMachine<PaymentState, PaymentEvent> authorizePayment(Long paymentId) {
		StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);
		sendEvent(paymentId, sm, PaymentEvent.AUTH_APPROVED);

		return null;
	}

	@Transactional
	@Override
	public StateMachine<PaymentState, PaymentEvent> declineAuth(Long paymentId) {

		StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);
		sendEvent(paymentId, sm, PaymentEvent.AUTHDECLINED);
		return null;
	}

	private void sendEvent(Long paymentId, StateMachine<PaymentState, PaymentEvent> sm, PaymentEvent event) {

		Message msg = MessageBuilder.withPayload(event).setHeader(PAYMENT_ID_HEADER, paymentId).build();

		sm.sendEvent(msg);

	}

	private StateMachine<PaymentState, PaymentEvent> build(Long paymentId) {
		Payment payment = paymentRepository.getOne(paymentId);
		StateMachine<PaymentState, PaymentEvent> sm = stateMachineFactory
				.getStateMachine(Long.toString(payment.getId()));
		sm.stop();
		sm.getStateMachineAccessor().doWithAllRegions(sma -> {
			sma.addStateMachineInterceptor(paymentStateChangeIntercepter);

			sma.resetStateMachine(
					new DefaultStateMachineContext<PaymentState, PaymentEvent>(payment.getState(), null, null, null));
		});

		sm.start();
		return sm;
	}

}
