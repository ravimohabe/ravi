package com.mindstix.statemachine.service;

import org.springframework.statemachine.StateMachine;

import com.mindstix.statemachine.configstatemachine.PaymentEvent;
import com.mindstix.statemachine.configstatemachine.PaymentState;
import com.mindstix.statemachine.model.Payment;

public interface PaymentService {

	Payment newPayment(Payment payment);
	
	StateMachine<PaymentState, PaymentEvent> preAuth(Long paymentId);
	
	StateMachine<PaymentState, PaymentEvent> authorizePayment(Long paymentId);
	
	StateMachine<PaymentState, PaymentEvent> declineAuth(Long paymentId);

	
}
