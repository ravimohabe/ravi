package com.mindstix.statemachine.configstatemachine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindstix.statemachine.model.Payment;
import com.mindstix.statemachine.service.PaymentService;

@RestController
public class Controller {
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	private StateMachineFactory<PaymentState,PaymentEvent > stateMachineFactory;
	
    final Payment payment = null;
	
	
	

	@RequestMapping("/hi")
	String hi()
	{
		payment.setAmount(500);
		paymentService.newPayment(payment);
		paymentService.preAuth((long) 100);
		paymentService.authorizePayment((long) 100);
		paymentService.declineAuth((long) 100);
		
		return "hello";
	}
	
}
