package com.mindstix.statemachine.configstatemachine;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Component;

import com.mindstix.statemachine.model.Payment;
import com.mindstix.statemachine.service.PaymentService;

@Component
@EnableAutoConfiguration
public class StateMachineStarter implements CommandLineRunner {

	
	@Override
	public void run(String... args) throws Exception {
	
	System.out.println("hello");	
	}

}
