package com.mindstix.statemachine.configstatemachine;

public enum PaymentEvent {
PRE_AUTHORISE,PRE_AUTH_APPROVED, PRE_AUTH_DECLINED,AUTHORIZE,AUTH_APPROVED,AUTHDECLINED
}
