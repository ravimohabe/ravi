package com.example.demo;

import java.util.EnumSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

@Configuration
@EnableStateMachine
public class StateMachineConfig
        extends EnumStateMachineConfigurerAdapter<States, Events> {

    @Override
    public void configure(StateMachineConfigurationConfigurer<States, Events> config)
            throws Exception {
    	System.out.println("inside configuration config");
        config
            .withConfiguration()
                .autoStartup(true)
                .listener(listen());
        
        System.out.println("inside configuration config");
    }

    @Override
    public void configure(StateMachineStateConfigurer<States, Events> states)
            throws Exception {
    	System.out.println("inside state config");
        states
            .withStates()
                .initial(States.S1)
                    .states(EnumSet.allOf(States.class));
        System.out.println("inside state config");
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<States, Events> transitions)
            throws Exception {
    	System.out.println("inside transition config");
        transitions
            .withExternal()
                .source(States.S1).target(States.S2).event(Events.E1)
                .and()
            .withExternal()
                .source(States.S2).target(States.S3).event(Events.E2);
        System.out.println("inside transition config");
    }

    @Bean
    public StateMachineListener<States, Events> listen() {
        return new StateMachineListenerAdapter<States, Events>() {
            @Override
            public void stateChanged(State<States, Events> from, State<States, Events> to) {
                System.out.println("State change to " + to.getId());
            }
        };
    }
}