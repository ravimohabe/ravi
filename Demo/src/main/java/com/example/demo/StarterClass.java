package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

@Component
public class StarterClass implements CommandLineRunner {
	private static Logger Log = LoggerFactory.getLogger(StarterClass.class);

	@Autowired
	private StateMachine<States, Events> stateMachine;

	@Override
	public void run(String... args) throws Exception {

		Log.info("Hi Application's run method implemented ");

		System.out.println("Hi Ravi");
	//	stateMachine.start();
//		stateMachine.sendEvent(Events.E1);
		
	}

}
