package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class Consumer {

private static Logger log=LoggerFactory.getLogger(Consumer.class);
	@Autowired
	private StateMachine<States, Events> stateMachine;

	@RabbitListener(queues = "${rabbitmq.queue}")
	public void receiveEvent(String Event) {
		log.info("Event received: {}",Event);
		Events e = Events.valueOf(Event);
		stateMachine.sendEvent(e);
	}
}
